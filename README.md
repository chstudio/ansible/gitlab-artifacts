Ansible Role to manage gitlab artifacts
---------------------------------------

This role allow to download a gitlab artifact from the currently running pipeline
or the configured one.

## Example

```ansible-playbook
- name: Install composer vendor
  include_role:
    name: gitlab-artifacts
  vars:
    job_name: "prepare:asset"
    dest: /home/www
    git_reference: my-branch
    api_token: TOKEN
    project_id: ABCD
    pipeline_id: 123
```
